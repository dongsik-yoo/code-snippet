// 전역 변수 부분
const SPREADSHEET_ID = "1lnhf3CY9p9LURoM6YAhcvc2CJQOo4sNfsAaqEjbMxr4";
const SOURCE_SHEET_LIST = ["Dooray_Web", "approval", "my-account", "log", "dooray.com", "TOAST_IAM", "workplace.toast.com", "Messenger_Electron", "Dooray_Mail_Template"]
const TRANSLATION_SHEET_NAME = "번역시트";
const TEN_MINUTE = 10 * 60 * 1000;
let lastGatherRun = Date.now();

const KO_KR = 'ko_KR';
const EN_US = 'en_US';
const JA_JP = 'ja_JP';
const ZH_CN = 'zh_CN';
const LANG_LIST = [KO_KR, EN_US, JA_JP, ZH_CN];

const sourceSheetIndexMap = {
  "service": 0,
  "context": 1,
  "key": 2,
  "ko_KR": 3,
  "en_US": 4,
  "ja_JP": 5,
  "zh_CN": 6,
  "comment": 7,
  "deleted": 8
};
const translationSheetIndexMap = {
  "platform": 0,
  "service": 1,
  "context": 2,
  "key": 3,
  "ko_KR": 4,
  "en_US": 5,
  "ja_JP": 6,
  "zh_CN": 7,
  "checkbox": 8
};

var isRunning = false;

function onChange() {
  var spreadsheet = SpreadsheetApp.openById(SPREADSHEET_ID);
  const translationSheet = spreadsheet.getSheetByName(TRANSLATION_SHEET_NAME);

  if (isRunning || !translationSheet.getRange("A1:A1").getValue()) {
    if (lastGatherRun - Date.now() > TEN_MINUTE) {
      isRunning = false;
    }
    return;
  }
  lastGatherRun = Date.now();
  isRunning = true;

  console.log('commit 반영 시작');
  handleAllCommit(spreadsheet, translationSheet, true);
  console.log('commit 반영 종료');

  const langDictionary = new Dictionary();
  let newRowIndex = 1;
  const notTranslateRows = SOURCE_SHEET_LIST.reduce((result, sheetName) => {
    console.log(sheetName, '분석 시작');
    const sheet = spreadsheet.getSheetByName(sheetName);
    const values = sheet.getRange("B2:J" + sheet.getMaxRows()).getValues();

    console.log(sheetName, '사전제작 시작');
    langDictionary.appendValues(values);
    console.log(sheetName, '사전제작 종료');
    const notTranslateRows = values.filter((row, index) => {
      
      if (!isNotTranslated(row)) {
        return false;
      }

      newRowIndex += 1;
      LANG_LIST.forEach(function (lang) {
        if (!getRowByKey(row, lang)) {
          return;
        }
        const matchRowList = langDictionary[lang][getRowByKey(row, lang)]
        if (matchRowList && matchRowList.length > 1) {
          matchRowList.splice(matchRowList.indexOf(row), 1);
          const note = matchRowList.map(rowToString).join('\n\n');
          translationSheet.getRange(newRowIndex, sourceSheetIndexMap[lang] + 2).setNote(note);
        }
      });
      row.unshift(sheetName + ":" + (index + 2));
      row.length = row.length - 2;
      return true;
    });
    console.log(sheetName, '분석 종료');
    return result.concat(notTranslateRows);
  }, []);
  
  if (notTranslateRows.length === 0) {
    translationSheet.getRange("A1:A1").setValue(false);
    return;
  }
  var translationSheetRange = "A2:H" + (notTranslateRows.length + 1);
  translationSheet.getRange(translationSheetRange).setValues(notTranslateRows);
  translationSheet.getRange("A1:A1").setValue(false);
  isRunning = false;
 }

 class Dictionary {
  constructor() {
    LANG_LIST.forEach(lang => this[lang] = {});
  }

  findDictionary(lang, rowValue) {
    return this[lang][rowValue];
  }

  appendValues(values) {
    values.forEach(row => {
      if (!isAllTranslated(row)) {
        return;
      }
      LANG_LIST.forEach(lang => {
        this.appendLangRow(lang, row);
      });
    });
  }

  appendLangRow(lang, row) {
    const rowValue = getRowByKey(row, lang);
    this[lang][rowValue] = this[lang][rowValue] || [];
    this[lang][rowValue].push(row);  
  }
}

function isNotTranslated(row) {
  const deleted = row[sourceSheetIndexMap.deleted];
  return (deleted === "FALSE" || deleted === false) && LANG_LIST.some(lang => !getRowByKey(row, lang));
}

function isAllTranslated(row) {
  return LANG_LIST.every(lang => !!getRowByKey(row, lang));
}

function getRowByKey(row, key) {
  return row[sourceSheetIndexMap[key]];
}

function rowToString(row) {
  return LANG_LIST.map(lang => getRowByKey(row, lang)).join('\t');
}
